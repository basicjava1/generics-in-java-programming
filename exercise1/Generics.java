package exercise1;

import java.util.ArrayList;
import java.util.List;

public class Generics {

    static <X> X doubleValue(X value){
        return value;
    }

    static <X extends List> void duplicate(X list){
        list.addAll(list);
    }

    public static void main(String[] args) {

        String value1 = doubleValue(new String());
        Integer number1 = doubleValue(Integer.valueOf(5));
        ArrayList list1 = doubleValue(new ArrayList());

        ArrayList<Integer> numbers = new ArrayList<>(List.of(1,2,3));
        duplicate(numbers);
        System.out.println(numbers);

        CustomList<String> list = new CustomList();
        list.addElement("Guitar");
        list.addElement("Tabla");
        String value = list.get(0);
        System.out.println(value);

        CustomList<Integer> list2 = new CustomList();
        list2.addElement(Integer.valueOf(5));

        Integer number = list2.get(0);
        System.out.println(number);
    }
}
